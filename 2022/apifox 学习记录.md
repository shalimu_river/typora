---

---

# 2020-07-23

# 智能Mock：生成数据

![image-20220723104009167](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723104009167.png)

![image-20220723104127822](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723104127822.png)

![image-20220723104204167](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723104204167.png)

## 复制URL 可以在浏览器里查看数据：刷新还可以出来新数据

![image-20220723104555209](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723104555209.png)

![image-20220723104620638](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723104620638.png)

## 通过自动导入也可以生成Mock数据

![image-20220723104839509](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723104839509.png)

# 自定义智能Mock:设置匹配规则，再设置对应的Mock规则

![image-20220723105206800](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105206800.png)

![image-20220723105239938](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105239938.png)

## 新建Mock规则

![image-20220723105425281](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105425281.png)

![image-20220723105520537](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105520537.png)

![image-20220723105545554](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105545554.png)

# 返回字段高级设置：响应枚举

![image-20220723105719768](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105719768.png)

![image-20220723105754828](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105754828.png)

![image-20220723105835615](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723105835615.png)

![image-20220723110010587](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723110010587.png)

# 接口级自定义Mock

## 写死返回字段

![image-20220723110504949](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723110504949.png)

## 返回字段设置跟预设的不匹配

![image-20220723110645215](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723110645215.png)

![image-20220723110710406](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723110710406.png)

## 经常用的Mock

![image-20220723110832265](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723110832265.png)

## 自己可以改写Mock规则

### 网址：[Mock 语法 | Apifox 使用文档](https://www.apifox.cn/help/app/mock/mock-rules/)

![image-20220723110958919](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723110958919.png)

## 数据模型也可以写Mock规则

![image-20220723111217875](https://gitee.com/shalimu_river/typora/raw/master/2022/image-20220723111217875.png)



























































































































